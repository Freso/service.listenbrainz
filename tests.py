import unittest
import helpers


class TestListenBrainz(unittest.TestCase):
    """A set of unit tests for the ListenBrainz listener plugin.

    To execute, run

    $ python -m unittest tests
    """

    def testRemoteURL(self) -> None:
        assert helpers.is_local("http://www.google.com") is False
        assert helpers.is_local(
            ("rtmp://a.rtmp.youtube.com/videolive?"
             "ns=yt-live&id=123456&itag=35&"
             "signature=blahblahblah/yt-live.123456.35")
        ) is False

    def testLocalFileURLs(self) -> None:
        assert helpers.is_local("/local/path")
        assert helpers.is_local("nonesuch")
        assert helpers.is_local("file:///local/path")
        assert helpers.is_local("smb://server/share/artist/album/song.mp3")
        assert helpers.is_local(
            ("musicdb://artists/22/11/123.mp3"
             "?albumartistsonly=false&albumid=11&artistid=22"))
        assert helpers.is_local("musicdb://songs/123.mp3")

    def testLocalIPv4URLs(self) -> None:
        assert helpers.is_local("http://8.8.8.8/File.mp3") is False
        assert helpers.is_local("rtmp://8.8.8.8/File.mp3") is False

        assert helpers.is_local("http://127.0.4.5/File.mp3")
        assert helpers.is_local("http://192.168.0.10/File.mp3")
        assert helpers.is_local("http://10.0.2.8/File.mp3")
        assert helpers.is_local("http://172.16.6.89/File.mp3")
        assert helpers.is_local("http://172.25.6.89/File.mp3")

        assert helpers.is_local("http://192.168.0.7:8200/MediaItems/40.mp3")

        assert helpers.is_local("rtmp://127.0.4.5/File.mp3")
        assert helpers.is_local("rtmp://192.168.0.10/File.mp3")
        assert helpers.is_local("rtmp://10.0.2.8/File.mp3")
        assert helpers.is_local("rtmp://172.16.6.89/File.mp3")
        assert helpers.is_local("rtmp://172.25.6.89/File.mp3")

    def testLocalIPv6URLs(self) -> None:
        assert helpers.is_local("http://fe80::ffff:ffff:ffff:ffff/File.mp3")
        assert helpers.is_local("rtmp://fe80::ffff:ffff:ffff:ffff/File.mp3")

        assert helpers.is_local("http://fc00::ffff:ffff:ffff:ffff/File.mp3")
        assert helpers.is_local("rtmp://fc00::ffff:ffff:ffff:ffff/File.mp3")

        assert helpers.is_local(
            "http://2607:f8b0:4005:800::1006/File.mp3") is False
        assert helpers.is_local(
            "rtmp://2607:f8b0:4005:800::1006/File.mp3") is False

    def testDomainNameFromURL(self) -> None:
        assert helpers.url_domain(
            'https://open.spotify.com/track/5fEjp2F0Sqr9fMuLSaDqz0'
        ) == 'spotify.com'
        assert helpers.url_domain(
            'https://www.jamendo.com/playlist/500390216/electro-funk-frenzy'
        ) == 'jamendo.com'
        assert helpers.url_domain(
            'https://www.youtube.com/watch?v=JKFBiaoFHoY'
        ) == 'youtube.com'
        assert helpers.url_domain(
            ("rtmp://a.rtmp.youtube.com/videolive?"
             "ns=yt-live&id=123456&itag=35&"
             "signature=blahblahblah/yt-live.123456.35")
        ) == 'youtube.com'

    def testGetMusicService(self) -> None:
        assert helpers.get_music_service(
            'https://open.spotify.com/track/5fEjp2F0Sqr9fMuLSaDqz0'
        ) == ('spotify.com', 'Spotify')
        assert helpers.get_music_service(
            'https://www.jamendo.com/playlist/500390216/electro-funk-frenzy'
        ) == ('jamendo.com', 'Jamendo Music')
        assert helpers.get_music_service(
            'https://www.youtube.com/watch?v=JKFBiaoFHoY'
        ) == ('youtube.com', 'YouTube')
        assert helpers.get_music_service(
            ("rtmp://a.rtmp.youtube.com/videolive?"
             "ns=yt-live&id=123456&itag=35&"
             "signature=blahblahblah/yt-live.123456.35")
        ) == ('youtube.com', 'YouTube')

    def testGetUrlData(self) -> None:
        assert helpers.get_url_data(
            'https://open.spotify.com/track/5fEjp2F0Sqr9fMuLSaDqz0'
        ) == {
            'music_service': 'spotify.com',
            'music_service_name': 'Spotify',
            'origin_url': ('https://open.spotify.com/track/'
                           '5fEjp2F0Sqr9fMuLSaDqz0'),
        }
        assert helpers.get_url_data(
            'https://www.jamendo.com/playlist/500390216/electro-funk-frenzy'
        ) == {
            'music_service': 'jamendo.com',
            'music_service_name': 'Jamendo Music',
            'origin_url': ('https://www.jamendo.com/playlist/'
                           '500390216/electro-funk-frenzy'),
        }
        assert helpers.get_url_data(
            'https://www.youtube.com/watch?v=JKFBiaoFHoY'
        ) == {
            'music_service': 'youtube.com',
            'music_service_name': 'YouTube',
            'origin_url': 'https://www.youtube.com/watch?v=JKFBiaoFHoY',
        }
        assert helpers.get_url_data(
            ("rtmp://a.rtmp.youtube.com/videolive?"
             "ns=yt-live&id=123456&itag=35&"
             "signature=blahblahblah/yt-live.123456.35")
        ) == {
            'music_service': 'youtube.com',
            'music_service_name': 'YouTube',
            'origin_url': ("rtmp://a.rtmp.youtube.com/videolive?"
                           "ns=yt-live&id=123456&itag=35&"
                           "signature=blahblahblah/yt-live.123456.35"),
        }


if __name__ == '__main__':
    unittest.main()
